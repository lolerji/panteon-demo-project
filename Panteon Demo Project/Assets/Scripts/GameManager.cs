﻿using PanteonDemo.Configuration.Base;
using PanteonDemo.Grid;
using UnityEngine;

namespace PanteonDemo
{
    public class GameManager : MonoBehaviour
    {
        private void Awake()
        {
            Dependencies();
            Configuration();
        }

        /// <summary>
        /// Creates objects that are considered dependencies, such as managers, singletons, containers and factories
        /// </summary>
        private void Dependencies()
        {
            // Create grid
            WorldGrid.Singletonize();
        }

        /// <summary>
        /// Configures various objects related to the game, such as dependency objects
        /// </summary>
        private void Configuration()
        {
            // Get configurations
            var configurations = GetComponents<IConfiguration>();

            // Run every configuration
            foreach (var configuration in configurations)
                configuration.Configure();
        }
    }
}
