﻿using PanteonDemo.Components;
using PanteonDemo.Data;
using PanteonDemo.Factory.Interface;
using PanteonDemo.Pooling;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.Factory
{
    public class ActorFactory : IActorFactory
    {
        public List<ActorInfo> actorList;

        #region Singleton

        private static ActorFactory m_Instance;

        private ActorFactory() { }
        
        /// <summary>
        /// Creates or gets the singleton instance of Actor Factory
        /// </summary>
        /// <returns></returns>
        public static ActorFactory Singletonize()
        {
            if (m_Instance.IsNull())
            {
                m_Instance = CreateInstance<ActorFactory>();
            }

            return m_Instance;
        }

        #endregion

        public override ActorComponent Create(string descriptor)
        {
            foreach (var infoParameter in actorList)
            {
                if (infoParameter.GetDescriptor() == descriptor)
                {
                    return Instantiate(infoParameter.prefab).GetComponent<ActorComponent>();
                }
            }

            return null;
        }
    }

    [System.Serializable]
    public class ActorInitializationParameter
    {
        public ActorInfo info;

        public GameObject prefab;
    }
}
