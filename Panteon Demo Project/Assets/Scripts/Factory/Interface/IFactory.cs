﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.Factory.Interface
{
    public abstract class IFactory<TRet, TDesc> : ScriptableObject
    {
        public abstract TRet Create(TDesc descriptor);
    }
}
