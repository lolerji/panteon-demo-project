﻿using PanteonDemo.Components;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.Factory.Interface
{
    public abstract class IActorFactory : IFactory<ActorComponent, string> { }
}
