﻿using PanteonDemo.Components;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.Containers
{
    public class ActorContainer : IActorContainer
    {
        private HashSet<ActorComponent> m_Actors;

        #region Singleton

        private static ActorContainer m_Instance;

        private ActorContainer()
        {
            m_Actors = new HashSet<ActorComponent>();
        }
        
        /// <summary>
        /// Creates or gets the singleton instance of Actor Container
        /// </summary>
        /// <returns></returns>
        public static ActorContainer Singletonize()
        {
            if (m_Instance.IsNull())
            {
                m_Instance = CreateInstance<ActorContainer>();
            }

            return m_Instance;
        }

        #endregion

        public override void AddInstance(ActorComponent component)
        {
            m_Actors.Add(component);
        }

        public override void RemoveInstance(ActorComponent component)
        {
            m_Actors.Remove(component);
        }

        public override ActorComponent GetActor(Vector3 position)
        {
            foreach (var actor in m_Actors)
            {
                if (actor.BindingVolume.bounds.Contains(position))
                {
                    return actor;
                }
            }

            return null;
        }

        public override IEnumerable<ActorComponent> EnumerateIntersectingActors(Bounds bounds)
        {
            foreach (var actor in m_Actors)
            {
                if (actor.BindingVolume.bounds.Intersects(bounds))
                {
                    yield return actor;
                }
            }
        }

        public override IEnumerable<ActorComponent> EnumerateActors()
        {
            foreach (var actorComponent in m_Actors)
                yield return actorComponent;
        }
    }
}
