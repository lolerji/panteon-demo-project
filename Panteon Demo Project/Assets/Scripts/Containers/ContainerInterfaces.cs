﻿using PanteonDemo.Components;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.Containers
{
    public abstract class IActorContainer : ScriptableObject
    {
        /// <summary>
        /// Adds and instance of actor component to the container
        /// </summary>
        /// <param name="component"></param>
        public abstract void AddInstance(ActorComponent component);

        /// <summary>
        /// Removes an instance of actor component from the container
        /// </summary>
        /// <param name="component"></param>
        public abstract void RemoveInstance(ActorComponent component);

        /// <summary>
        /// Returns the first actor component of an actor object that 
        /// covers the given position
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public abstract ActorComponent GetActor(Vector3 position);

        /// <summary>
        /// Gets the actor components of the all actors that is
        /// currently intersecting with given bounds
        /// </summary>
        /// <param name="bounds"></param>
        /// <returns></returns>
        public abstract IEnumerable<ActorComponent> EnumerateIntersectingActors(Bounds bounds);

        /// <summary>
        /// Gets an enumerated list of actor components that is
        /// currently recorded in the container
        /// </summary>
        /// <returns></returns>
        public abstract IEnumerable<ActorComponent> EnumerateActors();
    }
}
