﻿using PanteonDemo.Components;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.Pooling
{
    public abstract class IPool<TKey> : ScriptableObject
    {
        /// <summary>
        /// Initialies a pool of objects with given size, using the key parameter
        /// as the descriptor of the initialized pool
        /// </summary>
        /// <param name="key"></param>
        /// <param name="size"></param>
        public abstract void Initialize(TKey key, Transform parent, int size);

        /// <summary>
        /// Returns an available object from the pool described by the key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public abstract GameObject RequestObject(TKey key);

        /// <summary>
        /// Pushes an object back to the pool identified by the key
        /// </summary>
        /// <param name="key"></param>
        /// <param name="obj"></param>
        public abstract void ReturnObject(TKey key, GameObject obj);

        /// <summary>
        /// Clears the pool
        /// </summary>
        public abstract void Clear();
    }
}
