﻿using PanteonDemo.Data;
using PanteonDemo.Factory.Interface;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.Pooling
{
    public abstract class IActorPool : IPool<string> { }

    public class ActorPool : IActorPool
    {
        private Dictionary<string, Queue<GameObject>> m_Pools;
        private IActorFactory m_ActorFactory;

        #region Singleton

        private static ActorPool m_Instance;

        private ActorPool()
        {
            m_Pools = new Dictionary<string, Queue<GameObject>>();
        }
        
        /// <summary>
        /// Creates or gets the singleton instance of ActorPool
        /// </summary>
        /// <returns></returns>
        public static ActorPool Singletonize()
        {
            if (m_Instance.IsNull())
            {
                m_Instance = CreateInstance<ActorPool>();
            }

            return m_Instance;
        }

        #endregion

        public override void Clear()
        {
            // Loop all queues
            foreach (var queue in m_Pools.Values)
            {
                // And clear them
                ClearQueue(queue);
            }
        }

        public override void Initialize(string key, Transform parent, int size)
        {
            m_ActorFactory = m_ActorFactory ?? FindObjectOfType<IActorFactory>();

            Queue<GameObject> queue = null;

            // Make sure queue is initialized and clear
            if (m_Pools.TryGetValue(key, out queue))
            {
                ClearQueue(queue);
            }
            else
            {
                queue = new Queue<GameObject>();
                m_Pools[key] = queue;
            }

            // Initialize pool queue
            while (queue.Count < size)
            {
                var obj = m_ActorFactory.Create(key).gameObject;
                obj.transform.parent = parent;
                obj.SetActive(false);
                queue.Enqueue(obj);
            }
        }

        public override GameObject RequestObject(string key)
        {
            Queue<GameObject> queue = null;
            
            if (m_Pools.TryGetValue(key, out queue))
            {
                // Make sure object is activated before returning
                var obj = queue.Dequeue();
                obj.SetActive(true);
                return obj;
            }

            return null;
        }

        public override void ReturnObject(string key, GameObject obj)
        {
            Queue<GameObject> queue = null;

            if (m_Pools.TryGetValue(key, out queue))
            {
                // Object should be deactivated before
                // being pushed back into the pool
                obj.SetActive(false);
                queue.Enqueue(obj);
            }
        }

        /// <summary>
        /// Clears a queue of Unity Game Objects, destroying
        /// each game object in the queue as well
        /// </summary>
        /// <param name="queue"></param>
        private void ClearQueue(Queue<GameObject> queue)
        {
            // Destroy all game objects of each queue
            while (queue.Count > 0)
                Destroy(queue.Dequeue());
        }
    }
}