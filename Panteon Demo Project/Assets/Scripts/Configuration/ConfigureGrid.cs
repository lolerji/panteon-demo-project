﻿using PanteonDemo.Configuration.Base;
using PanteonDemo.Grid;
using UnityEngine;

namespace PanteonDemo.Configuration
{
    public class ConfigureGrid : ConfigurationBase
    {
        [SerializeField] private float cellWidth;
        [SerializeField] private float cellHeight;

        [SerializeField] private Vector3 xAxis;
        [SerializeField] private Vector3 yAxis;
        [SerializeField] private Vector3 origin;

        public override void Configure()
        {
            IGrid grid = FindObjectOfType<IGrid>();

            if (grid.IsNull())
            {
                Debug.LogError("Configuration of the game requires an IGrid object to work correctly", this);
                return;
            }

            // Set cell size
            grid.SetCellSize(cellWidth, cellHeight);

            // Set origin
            grid.SetGridOrigin(origin);

            // Set axes
            grid.SetXAxis(xAxis);
            grid.SetYAxis(yAxis);
        }
    }
}
