﻿using PanteonDemo.Configuration.Base;
using PanteonDemo.Data;
using PanteonDemo.Factory.Interface;
using PanteonDemo.Pooling;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.Configuration
{
    public class ConfigurePooling : ConfigurationBase
    {
        [SerializeField] private Vector3Int expectedGridSize;
        [SerializeField] private List<ActorPoolInitializationParameter> actorPoolParameters;

        public override void Configure()
        {
            var pool = ActorPool.Singletonize();

            foreach (var param in actorPoolParameters)
            {
                int size = (int)(expectedGridSize.x * expectedGridSize.z / (param.info.dimensions.x * param.info.dimensions.z));
                pool.Initialize(param.info.GetDescriptor(), param.parent, size);
            }
        }
    }

    [System.Serializable]
    class ActorPoolInitializationParameter
    {
        public string name;

        public ActorInfo info;

        public Transform parent;
    }
}
