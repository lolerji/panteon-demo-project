﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.Configuration.Base
{
    public abstract class ConfigurationBase : MonoBehaviour, IConfiguration
    {
        /// <summary>
        /// Provides configuration
        /// </summary>
        public abstract void Configure();
    }
}