﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.Configuration.Base
{
    public interface IConfiguration
    {
        void Configure();
    }
}
