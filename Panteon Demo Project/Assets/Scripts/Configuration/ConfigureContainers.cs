﻿using PanteonDemo.Configuration.Base;
using PanteonDemo.Containers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.Configuration
{
    public class ConfigureContainers : MonoBehaviour, IConfiguration
    {
        public void Configure()
        {
            // Create actors container
            var actorsContainer = ActorContainer.Singletonize();
        }
    }
}
