﻿using PanteonDemo.Configuration.Base;
using PanteonDemo.Data;
using PanteonDemo.Factory;
using PanteonDemo.Pooling;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.Configuration
{
    public class ConfigureFactory : ConfigurationBase
    {
        [SerializeField] private List<ActorInfo> actorInfoList;

        public override void Configure()
        {
            // Create actor factory
            var actorFactory = ActorFactory.Singletonize();
            actorFactory.actorList = actorInfoList;
        }
    }
}
