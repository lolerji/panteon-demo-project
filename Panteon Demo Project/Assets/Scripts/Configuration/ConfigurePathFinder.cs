﻿using PanteonDemo.Configuration.Base;
using PanteonDemo.PathFinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.Configuration
{
    public class ConfigurePathFinder : ConfigurationBase
    {
        [SerializeField] private LayerMask layerMask;
        [SerializeField] private Vector3 upwardsVector;

        public override void Configure()
        {
            var pathFinder = AStarActorGridPathFinder.Singletonize();
            pathFinder.ConfigurePathFinder(layerMask, upwardsVector);
        }
    }
}
