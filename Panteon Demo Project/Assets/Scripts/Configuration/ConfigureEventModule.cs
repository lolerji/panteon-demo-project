﻿using PanteonDemo.Configuration.Base;
using PanteonDemo.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.Configuration
{
    public class ConfigureEventModule : ConfigurationBase
    {
        public override void Configure()
        {
            EventModule.Singletonize();
        }
    }
}
