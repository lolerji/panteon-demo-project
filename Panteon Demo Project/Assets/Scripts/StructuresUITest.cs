﻿using PanteonDemo.UI.Controller;
using PanteonDemo.UI.View;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StructuresUITest : MonoBehaviour {

    [SerializeField] private ConstructionController constructionController;
    [SerializeField] private InfiniteScrollView view;

    public void OpenListFromStart()
    {
        constructionController.ConstructionList(0, view.PageSize);
    }
}
