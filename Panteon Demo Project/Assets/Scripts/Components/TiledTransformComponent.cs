﻿using PanteonDemo.Grid;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.Components
{
    public class TiledTransformComponent : MonoBehaviour
    {
        public virtual Vector3 Dimensions
        {
            get { return transform.localScale; }
        }
    }
}
