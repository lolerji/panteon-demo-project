﻿using PanteonDemo.Components.Interface;
using PanteonDemo.Components.Modes;
using PanteonDemo.Data;
using PanteonDemo.Events.EventTypes;
using PanteonDemo.Events.Interface;
using PanteonDemo.Pooling;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace PanteonDemo.Components
{
    public class InteractionControllerComponent : MonoBehaviour
    {
        private IModeComponent<string> m_PreviewMode;
        private IModeComponent<UnitComponent> m_UnitMovementMode;
        private IEventModule m_EventModule;

        private IModeComponent m_CurrentMode;

        private void Start()
        {
            m_PreviewMode = FindObjectOfType<IModeComponent<string>>();
            m_UnitMovementMode = FindObjectOfType<IModeComponent<UnitComponent>>();
        }

        private void Update()
        {
            // Current mode is active and has update logic
            if (!m_CurrentMode.IsNull() && !m_CurrentMode.ModeUpdate.IsNull() && m_CurrentMode.IsActive)
            {
                m_CurrentMode.ModeUpdate.Invoke();
            }

            // If escape is pressed, deactivate currently active mode
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                DeactivateActiveMode();
            }
        }

        private void OnEnable()
        {
            // Register event listener methods
            m_EventModule = m_EventModule ?? FindObjectOfType<IEventModule>();
            m_EventModule.AddListener<StructureListItemClicked>(EnableConstrcutionPreviewMode);
            m_EventModule.AddListener<ActorClicked>(EnableUnitMovementMode);
        }

        private void OnDisable()
        {
            // Unregister event listener methods
            m_EventModule = m_EventModule ?? FindObjectOfType<IEventModule>();
            m_EventModule.AddListener<StructureListItemClicked>(EnableConstrcutionPreviewMode);
            m_EventModule.AddListener<ActorClicked>(EnableUnitMovementMode);
        }

        private void DeactivateActiveMode()
        {
            // Mode deactivation check
            if (!m_CurrentMode.IsNull() && m_CurrentMode.IsActive)
            {
                m_CurrentMode.DeactivateMode();
            }
        }


        // -------------------------------------------------------- //
        //                      EVENT LISTENERS                     //
        // -------------------------------------------------------- //


        private void EnableConstrcutionPreviewMode(StructureListItemClicked data)
        {
            // Deactivate previous mode automatically
            DeactivateActiveMode();

            // Enable construction preview mode
            m_PreviewMode.EnableMode(data.StructureName);
            m_CurrentMode = m_PreviewMode;      // Set construction preview mode as current mode
        }

        private void EnableUnitMovementMode(ActorClicked data)
        {
            // Deactivate previous mode automatically
            DeactivateActiveMode();

            // Enable unit movement mode if the actor is a unit
            if (data.Actor.GetActorType() == ActorType.Unit)
            {
                // Enable unit movement mode
                m_UnitMovementMode.EnableMode(data.GameObject.GetComponent<UnitComponent>());
                m_CurrentMode = m_UnitMovementMode;     // Set unit movement mode as current mode
            }
        }
    }
}
