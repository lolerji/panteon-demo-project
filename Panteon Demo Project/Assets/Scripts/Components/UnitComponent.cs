﻿using PanteonDemo.Data;
using PanteonDemo.Events.EventTypes;
using PanteonDemo.Events.Interface;
using PanteonDemo.PathFinding.Interface;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace PanteonDemo.Components
{
    public class UnitComponent : ActorComponent
    {
        private IPathFinder m_PathFinder;

        private IEnumerable<Vector3> m_Path;
        private Vector3 m_CurrentGoal;
        private Coroutine m_Coroutine;

        protected void Start()
        {
            m_PathFinder = FindObjectOfType<IPathFinder>();
            m_CurrentGoal = transform.position;
        }

        private void Update()
        {
            // Translate position only if actor is following a path (Follow path coroutine is not null)
            if (!m_Coroutine.IsNull())
            {
                transform.rotation = Quaternion.LookRotation(m_CurrentGoal - transform.position, transform.up);
                transform.position = Vector3.Lerp(transform.position, m_CurrentGoal, Time.deltaTime * Unit.speed);
            }
        }

        private IEnumerator FollowPath()
        {
            foreach (var position in m_Path)
            {
                m_CurrentGoal = position;

                while (Vector3.Distance(transform.position, m_CurrentGoal) > .1f * Unit.speed)
                {
                    yield return null;
                }

                yield return null;
            }

            m_Coroutine = null;
            yield return null;
        }

        public void SetGoal(Vector3 goal)
        {
            // Stop previous follow path coroutine
            if (!m_Coroutine.IsNull())
            {
                StopCoroutine(m_Coroutine);
            }

            // Find new path
            m_Path = m_PathFinder.FindPath(transform.position, goal);
            m_Coroutine = StartCoroutine(FollowPath());
        }

        public UnitInfo Unit
        {
            get { return Info as UnitInfo; }
        }
    }
}
