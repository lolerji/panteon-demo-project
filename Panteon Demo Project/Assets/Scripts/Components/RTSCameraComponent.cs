﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.Components
{
    public class RTSCameraComponent : MonoBehaviour
    {
        [SerializeField] private float cameraSpeed;
        [SerializeField] [Range(1, 10)] private float damping = 1;
        [SerializeField] private float volumeHalfSizeHorizontal;
        [SerializeField] private float volumeHalfSizeVertical;
        [SerializeField] private Vector3 groundVertical;
        [SerializeField] private Vector3 groundHorizontal;

        private Vector3 m_StartPosition;

        private void Start()
        {
            m_StartPosition = transform.position;
        }

        private void LateUpdate()
        {
            // If middle mouse button is being pressed
            if (Input.GetMouseButton(2))
            {
                // Get mouse delta
                float hDelta = Input.GetAxis("Mouse X");
                float vDelta = Input.GetAxis("Mouse Y");

                // Calculate movement vector
                var deltaMovement = hDelta * Horizontal + vDelta * Vertical;

                // Apply interpolated movement
                var backupCurrentPosition = transform.position;
                transform.position = Vector3.Lerp(transform.position, transform.position + deltaMovement * cameraSpeed, Time.deltaTime / damping);

                // If position delta exceeds limit
                if (!IsInBindingVolume(transform.position - m_StartPosition))
                {
                    // Restore position
                    transform.position = backupCurrentPosition;
                }
            }
        }

        private bool IsInBindingVolume(Vector3 totalDelta)
        {
            // Check if both horizontal and vertical components of the delta vector
            // is in the bounds of the volume and return true if it is inside the volume,
            // or false otherwise
            return Vector3.Scale(totalDelta, groundVertical).magnitude < volumeHalfSizeVertical
                && Vector3.Scale(totalDelta, groundHorizontal).magnitude < volumeHalfSizeHorizontal;
        }

        /// <summary>
        /// Get "right" vector of transform projected to the ground plane
        /// </summary>
        private Vector3 Horizontal
        {
            get { return Vector3.Scale(transform.right, groundVertical + groundHorizontal); }
        }

        /// <summary>
        /// Get "forward" vector of transform projected to the ground plane
        /// </summary>
        private Vector3 Vertical
        {
            get { return Vector3.Scale(transform.forward, groundVertical + groundHorizontal); }
        }
    }
}
