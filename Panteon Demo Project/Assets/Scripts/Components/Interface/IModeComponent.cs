﻿using PanteonDemo.Pooling;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace PanteonDemo.Components.Interface
{
    public abstract class IModeComponent : MonoBehaviour
    {
        public abstract void DeactivateMode();

        public abstract UnityAction ModeUpdate { get; }

        public abstract bool IsActive { get; }
    }

    public abstract class IModeComponent<TParam> : IModeComponent
    {
        public abstract void EnableMode(TParam parameter);
    }
}
