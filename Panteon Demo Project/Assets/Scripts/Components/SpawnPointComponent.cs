﻿using System.Collections;
using System.Collections.Generic;
using PanteonDemo.Containers;
using PanteonDemo.Data;
using PanteonDemo.Events.EventTypes;
using PanteonDemo.Events.Interface;
using PanteonDemo.Factory.Interface;
using UnityEngine;

namespace PanteonDemo.Components
{
    public class SpawnPointComponent : ActorComponent
    {
        private IActorFactory m_ActorFactory;

        private void Start()
        {
            m_ActorFactory = FindObjectOfType<IActorFactory>();

            // Register spawnpoint as an actor, so player
            // won't be able to place anything on top of it
            FindObjectOfType<IActorContainer>().AddInstance(this);
        }

        private void OnEnable()
        {
            EventModule.AddListener<UnitListItemClicked>(SpawnUnit);
        }

        private void OnDisable()
        {
            EventModule.AddListener<UnitListItemClicked>(SpawnUnit);
        }

        private void SpawnUnit(UnitListItemClicked data)
        {
            var unit = m_ActorFactory.Create(data.UnitName);
            unit.transform.position = transform.position;
        }
    }
}
