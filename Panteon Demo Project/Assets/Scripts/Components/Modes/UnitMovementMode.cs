﻿using PanteonDemo.Components.Interface;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace PanteonDemo.Components.Modes
{
    public class UnitMovementMode : IModeComponent<UnitComponent>
    {
        [SerializeField] private Camera gameCamera;
        [SerializeField] private LayerMask raycastLayers;

        private UnitComponent m_CurrentUnit;

        public override void DeactivateMode()
        {
            m_CurrentUnit = null;
        }

        public override void EnableMode(UnitComponent parameter)
        {
            m_CurrentUnit = parameter;
        }

        private void SetClickedPositionAsGoal()
        {
            // If right mouse button is clicked
            if (Input.GetMouseButtonDown(1))
            {
                RaycastHit hit;

                // Raycast to ground and set hit position as goal to the unit
                if (Physics.Raycast(gameCamera.ScreenPointToRay(Input.mousePosition), out hit, raycastLayers.value))
                {
                    m_CurrentUnit.SetGoal(hit.point);
                }
            }
        }

        public override UnityAction ModeUpdate { get { return SetClickedPositionAsGoal; } }

        public override bool IsActive { get { return !m_CurrentUnit.IsNull(); } }
    }
}