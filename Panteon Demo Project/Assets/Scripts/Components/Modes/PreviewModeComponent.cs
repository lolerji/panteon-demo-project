﻿using PanteonDemo.Components.Interface;
using PanteonDemo.Data;
using PanteonDemo.Pooling;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace PanteonDemo.Components.Modes
{
    public class PreviewModeComponent : IModeComponent<string>
    {
        [SerializeField] private PreviewBoxComponent previewBox;

        private IActorPool m_ActorPool;

        private void Start()
        {
            m_ActorPool = FindObjectOfType<IActorPool>();
        }

        public override void EnableMode(string parameter)
        {
            // Get a new actor instance
            var actor = m_ActorPool.RequestObject(parameter);

            // Pass new actor to preview box for placement and previewing
            previewBox.SetPreviewActor(actor.GetComponent<ActorComponent>());
            previewBox.Activate();      // Activate preview box
        }

        public override void DeactivateMode()
        {
            // Deactivate preview box
            previewBox.Deactivate();

            // Return actor that is being previewed, if there is one
            var previewing = previewBox.GetPreviewActor();
            if (!previewing.IsNull())
            {
                previewBox.Deactivate();
                previewBox.SetPreviewActor(null);
                m_ActorPool.ReturnObject(previewing.Info.GetDescriptor(), previewing.gameObject);
            }
        }

        // Preview mode update logic is handled by Preview box component!
        // So we return null
        public override UnityAction ModeUpdate { get { return null; } }

        // Return preview mode state
        public override bool IsActive { get { return !previewBox.GetPreviewActor().IsNull(); } }
    }
}
