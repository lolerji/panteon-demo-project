﻿using PanteonDemo.Containers;
using PanteonDemo.Data;
using PanteonDemo.Events.EventTypes;
using PanteonDemo.Events.Interface;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace PanteonDemo.Components
{
    public class ActorComponent : TiledTransformComponent, IPointerClickHandler
    {
        [SerializeField] private ActorInfo actorInfo;
        [SerializeField] private Collider bindingVolume;

        private IActorContainer m_ActorContainer;
        private IEventModule m_EventModule;

        private void OnEnable()
        {
            m_ActorContainer = m_ActorContainer ?? FindObjectOfType<IActorContainer>();
            m_ActorContainer.AddInstance(this);
        }

        private void OnDisable()
        {
            m_ActorContainer = m_ActorContainer ?? FindObjectOfType<IActorContainer>();
            m_ActorContainer.RemoveInstance(this);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            // If actor is clicked with left button
            if (eventData.button == PointerEventData.InputButton.Left)
            {
                // Raise actor clicked event
                EventModule.Raise(new ActorClicked() {
                    Actor = actorInfo,
                    GameObject = gameObject
                });
            }
        }

        public virtual ActorInfo Info
        {
            get { return actorInfo; }
        }

        public Collider BindingVolume
        {
            get { return bindingVolume; }
        }

        public override Vector3 Dimensions
        {
            get { return actorInfo.dimensions; }
        }

        protected IEventModule EventModule
        {
            get
            {
                // Find instance of IEventModule if it is not yet found
                if (m_EventModule.IsNull())
                    m_EventModule = FindObjectOfType<IEventModule>();

                // Return instance of IEventModule
                return m_EventModule;
            }
        }
    }
}
