﻿using PanteonDemo.Components;
using PanteonDemo.Containers;
using PanteonDemo.Grid;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo
{
    public class PreviewBoxComponent : TiledTransformComponent
    {
        [SerializeField] private Color okColor;
        [SerializeField] private Color invalidColor;

        [Header("Raycasting")]
        [SerializeField] private Camera mainCamera;
        [SerializeField] private float rayCastLength;
        [SerializeField] private LayerMask gridLayers;

        private IActorContainer m_ActorContainer;
        private IGrid m_Grid;

        private Renderer m_Renderer;
        private ActorComponent m_Actor;
        private bool m_CanPlace;

        private void Start()
        {
            m_ActorContainer = FindObjectOfType<IActorContainer>();
            m_Grid = FindObjectOfType<IGrid>();

            m_Renderer = GetComponent<Renderer>();
        }

        private void Update()
        {
            foreach (var intersectingActor in m_ActorContainer.EnumerateIntersectingActors(m_Actor.BindingVolume.bounds))
            {
                if (!intersectingActor.IsNull())
                {
                    m_Renderer.material.color = invalidColor;
                    m_CanPlace = false;
                    return;
                }
            }

            m_Renderer.material.color = okColor;
            m_CanPlace = true;
        }

        private void LateUpdate()
        {
            // Get mouse delta
            float mouseX = Input.GetAxis("Mouse X");
            float mouseY = Input.GetAxis("Mouse Y");

            if (Input.GetMouseButtonDown(0) && m_CanPlace)
            {
                // If left mouse button is pressed and there are no obstacles, place the actor that is being previewed
                Deactivate();
                SetPreviewActor(null);
            }
            else if (Mathf.Abs(mouseX + mouseY) > 0.1f)
            {
                // If there is a delta, move the preview box on the grid
                RaycastHit hit;

                var ray = mainCamera.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hit, rayCastLength, gridLayers.value))
                {
                    this.SetTiledTransformPosition(m_Grid, m_Grid.Cell(hit.point));
                }
            }
        }

        /// <summary>
        /// Activates the preview box to show
        /// the area which the actor will cover,
        /// and whether the actor can be placed there
        /// 
        /// IMPORTANT! Must be called after a set preview actor call
        /// </summary>
        public void Activate()
        {
            // Enable this
            gameObject.SetActive(true);

            // Disable actor
            m_Actor.enabled = false;

            // Scale the preview box to fit actor's tiling
            transform.localScale = m_Actor.Dimensions;

            // Set preview box as parent of the actor
            m_Actor.transform.parent = transform;
            m_Actor.transform.localPosition = Vector3.zero;
        }

        /// <summary>
        /// Deactivates the preview 
        /// </summary>
        public void Deactivate()
        {
            gameObject.SetActive(false);

            // Removes previous actor from preview box
            // if there was a previous preview actor
            m_Actor.transform.parent = null;
            m_Actor.enabled = true;
        }

        public void SetPreviewActor(ActorComponent actor)
        {
            // Set actor
            m_Actor = actor;
        }

        public ActorComponent GetPreviewActor()
        {
            return m_Actor;
        }
    }
}
