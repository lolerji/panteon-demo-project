﻿using PanteonDemo.Data;
using PanteonDemo.UI.Base;
using PanteonDemo.UI.Interface;
using PanteonDemo.UI.Model;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using UnityEngine;

namespace PanteonDemo.UI.Controller
{
    public class ConstructionController : ControllerBase
    {
        [Header("Data")]
        [SerializeField] private StructureList structures;

        [Header("Views")]
        [SerializeField] private IView constructionView;

        /// <summary>
        /// Displays a certain number of constructable structures 
        /// starting from the given starting index on UI
        /// </summary>
        /// <param name="start">Starting index</param>
        /// <param name="count">Number of structures to display</param>
        public void ConstructionList(int start, int count)
        {
            // Populate constructable structure list from structure
            var model = new StructureListModel() {
                totalCount = structures.actors.Count,
                structures = structures.actors.Skip(start).Take(count).Select(s => new StructureModel() {
                    structureDescription = s.description,
                    structureName = s.actorName,
                    structureSprite = s.sprite
                })
            };

            // Dispatch model
            DispatchModel(constructionView, model);
        }

        public override void InvokeAction(string actionName, params object[] obj)
        {
            switch (actionName)
            {
                case nameof(ConstructionController.ConstructionList):
                    ConstructionList((int)obj[0], (int)obj[1]);
                    break;
            }
        }
    }
}
