﻿using PanteonDemo.Data;
using PanteonDemo.UI.Base;
using PanteonDemo.UI.Interface;
using PanteonDemo.UI.Model;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PanteonDemo.UI.Controller
{
    public class StructureController : ControllerBase
    {
        [Header("Data")]
        [SerializeField] private StructureList structures;

        [Header("Views")]
        [SerializeField] private IView structureDetailsView;
        [SerializeField] private IView productionListView;

        /// <summary>
        /// Displays the info of the given structure on UI
        /// </summary>
        /// <param name="structure"></param>
        public void StructureDetails(string structureDescriptor)
        {
            // Find actor in structures list
            var structure = structures.actors.Find(a => a.GetDescriptor() == structureDescriptor);

            if (structure.IsNull())
            {
                // If no structure was found, return null model
                DispatchModel(structureDetailsView, ModelBase.Null);
            }
            else
            {
                // Create model for the view
                var model = new StructureModel() {
                    structureDescription = structure.description,
                    structureName = structure.actorName,
                    structureSprite = structure.sprite
                };

                // Dispatch it
                DispatchModel(structureDetailsView, model);
            }
        }

        /// <summary>
        /// Displays the production list of the given structure on UI
        /// </summary>
        /// <param name="structure"></param>
        public void ProductionList(string structureDescriptor, int startIndex, int count)
        {
            // Find actor in structures list
            var structure = structures.actors.Find(a => a.GetDescriptor() == structureDescriptor);

            if (structure.IsNull() || structure.produces.Skip(startIndex).Take(count).Count() < 1)
            {
                // If no structure was found or the structure doesn't have
                // any more producable units from the start index,
                // return null
                DispatchModel(productionListView, ModelBase.Null);
            }
            else 
            {
                // Create and populate production list model using
                // the production list from structure info
                var model = new ProductionListModel() {
                    totalCount = structure.produces.Count(),
                    structureDescriptor = structure.GetDescriptor(),
                    units = structure.produces.Skip(startIndex).Select(s => new UnitModel() {
                        unitName = s.actorName,
                        unitSprite = s.sprite,
                        unitType = s.unitType
                    })
                };

                // Dispatch model
                DispatchModel(productionListView, model);
            }
        }

        public override void InvokeAction(string actionName, params object[] obj)
        {
            switch (actionName)
            {
                case nameof(StructureController.StructureDetails):
                    StructureDetails(obj[0] as string);
                    break;

                case nameof(StructureController.ProductionList):
                    ProductionList(obj[0] as string, (int)obj[1], (int)obj[2]);
                    break;
            }
        }
    }
}
