﻿using PanteonDemo.UI.Base;
using PanteonDemo.UI.Model;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace PanteonDemo.UI.View
{
    public class StructureDetailsView : TemplateView<StructureModel>
    {
        [SerializeField] private Text title;
        [SerializeField] private Image structureImage;

        protected override void UpdateViewInternal()
        {
            var model = GetModel();
            title.text = model.structureName;
            structureImage.sprite = model.structureSprite;
        }
    }
}
