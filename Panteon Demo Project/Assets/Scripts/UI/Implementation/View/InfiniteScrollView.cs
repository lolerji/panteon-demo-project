﻿using PanteonDemo.Events.EventTypes;
using PanteonDemo.Events.Interface;
using PanteonDemo.UI.Controller;
using PanteonDemo.UI.Interface;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace PanteonDemo.UI.View
{
    public class InfiniteScrollView : MonoBehaviour, IScrollHandler
    {
        [SerializeField] private GridLayoutGroup gridLayout;
        [SerializeField] private Transform layoutParent;
        [SerializeField] private GameObject itemPrefab;
        [SerializeField] private float scrollSensitivity;

        private UnityAction<int, int> m_ScrollCallback;
        private RectTransform m_GridRect;

        private int m_RowCount;
        private int m_ColumnCount;
        private int m_VisibleItemCount;
        private int m_CurrentTotal;
        private int m_TotalRows;
        private List<IView> m_Items;

        /// <summary>
        /// Sets the active state of the item by providing a default
        /// active/inactive behaviour for it
        /// </summary>
        /// <param name="item"></param>
        /// <param name="state"></param>
        protected virtual void SetItemState(GameObject item, bool state)
        {
            item.SetActive(state);
        }

        /// <summary>
        /// Calculates number of rows and columns and determines the number of visible objects there should be
        /// and instantiates that amount of items
        /// </summary>
        public void Adjust()
        {
            m_GridRect = m_GridRect ?? gridLayout.GetComponent<RectTransform>();

            // Calculate number of rows visible
            // on the screen
            float rows = (m_GridRect.rect.height - gridLayout.spacing.y) / gridLayout.cellSize.y;
            m_RowCount = Mathf.FloorToInt(rows);

            // Calculate total visible item count from number of rows
            float columns = (m_GridRect.rect.width - gridLayout.spacing.x) / gridLayout.cellSize.x;
            int childCount = transform.childCount;
            m_ColumnCount = Mathf.FloorToInt(columns);
            m_VisibleItemCount = m_ColumnCount * m_RowCount;

            // Instantiate items
            for (int i = childCount; i < m_VisibleItemCount; i++)
            {
                var item = Instantiate(itemPrefab, layoutParent);
                item.SetActive(false);
                Items.Add(item.GetComponent<IView>());
            }
        }

        public void SetTotalItemCount(int totalCount)
        {
            m_CurrentTotal = totalCount;
            m_TotalRows = Mathf.CeilToInt((float)m_CurrentTotal / m_ColumnCount);
        }

        /// <summary>
        /// Yields each item in the grid rect
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IView> EnumerateItems()
        {
            foreach (var item in Items)
            {
                yield return item;
            }
        }

        /// <summary>
        /// Gets the index of the given item
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int ItemIndex(IView item)
        {
            return Items.IndexOf(item);
        }

        /// <summary>
        /// Assign a callback method to be called when this scroll view is scrolled
        /// </summary>
        /// <param name="scrollAction"></param>
        public void SetScrollAction(UnityAction<int, int> scrollAction)
        {
            m_ScrollCallback = scrollAction;
        }

        /// <summary>
        /// Event callback for scrolling
        /// </summary>
        /// <param name="eventData"></param>
        public void OnScroll(PointerEventData eventData)
        {
            int currentRow = DataStart / m_ColumnCount;
            int deltaRow = Mathf.RoundToInt(scrollSensitivity * eventData.scrollDelta.y);
            int newRow = currentRow + deltaRow;
         
            // Make sure new row index is clamped between 0 and max row - rows per page.
            // This way, when we scroll, the last row will always remain at the bottom
            newRow = Mathf.Clamp(newRow, 0, m_TotalRows - m_RowCount);
            
            DataStart = newRow * m_ColumnCount;

            if (!m_ScrollCallback.IsNull())
                m_ScrollCallback.Invoke(DataStart, PageSize);
        }

        /// <summary>
        /// Gets the item at given index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public IView this[int index]
        {
            get
            {
                if (index > Items.Count)
                    return null;
                return Items[index];
            }
        }

        /// <summary>
        /// Returns the number of active items
        /// </summary>
        public int ActiveItemCount
        {
            get
            {
                return m_Items.Count(i => i.gameObject.activeInHierarchy);
            }
        }

        /// <summary>
        /// Return number of item views in the scroll view
        /// </summary>
        public int PageSize
        {
            get
            {
                return Items.Count;
            }
        }

        /// <summary>
        /// Start index of data
        /// </summary>
        public int DataStart { get; private set; }
        
        protected List<IView> Items
        {
            get
            {
                if (m_Items.IsNull())
                    m_Items = new List<IView>();
                return m_Items;
            }
        }
    }
}
