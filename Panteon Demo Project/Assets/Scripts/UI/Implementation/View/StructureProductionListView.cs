﻿using PanteonDemo.UI.Base;
using PanteonDemo.UI.Controller;
using PanteonDemo.UI.Model;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PanteonDemo.UI.View
{
    public class StructureProductionListView : TemplateView<ProductionListModel>
    {
        [SerializeField] private InfiniteScrollView productionList;

        private void Start()
        {
            productionList.SetScrollAction((start, count)
                => InvokeAction(nameof(StructureController.ProductionList), GetModel().structureDescriptor, start, count));
        }

        private void OnEnable()
        {
            productionList.Adjust();
        }

        protected override void UpdateViewInternal()
        {
            var model = GetModel();
            int unitCount = model.units.Count();
            productionList.SetTotalItemCount(model.totalCount);

            // Set all items of scroll view
            for (int i = 0; i < productionList.PageSize; i++)
            {
                var item = productionList[i];

                if (item.IsNull())
                    return;

                // If item has model
                if (i < unitCount)
                {
                    // Set item as active
                    item.gameObject.SetActive(true);

                    // Set model of the item
                    item.Model = model.units.ElementAt(i);
                    item.UpdateView();      // Update child view
                }
                else
                {
                    // Otherwise, set item as inactive
                    item.gameObject.SetActive(false);
                }

            }
        }
    }
}
