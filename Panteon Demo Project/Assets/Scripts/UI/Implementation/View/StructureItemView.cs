﻿using PanteonDemo.Events.EventTypes;
using PanteonDemo.Events.Interface;
using PanteonDemo.UI.Base;
using PanteonDemo.UI.Model;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PanteonDemo.UI.View
{
    public class StructureItemView : TemplateView<StructureModel>
    {
        [SerializeField] private Image imageView;

        private IEventModule m_EventModule;

        private void Start()
        {
            m_EventModule = FindObjectOfType<IEventModule>();
        }

        protected override void UpdateViewInternal()
        {
            imageView.sprite = GetModel().structureSprite;
        }

        public void OnClick()
        {
            var model = GetModel();

            // Raise structure item clicked event
            m_EventModule.Raise(new StructureListItemClicked() {
                StructureName = model.structureName,
                StructureType = model.structureType
            });
        }
    }
}
