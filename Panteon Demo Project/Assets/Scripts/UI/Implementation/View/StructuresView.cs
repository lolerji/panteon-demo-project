﻿using PanteonDemo.UI.Base;
using PanteonDemo.UI.Controller;
using PanteonDemo.UI.Interface;
using PanteonDemo.UI.Model;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace PanteonDemo.UI.View
{
    public class StructuresView : TemplateView<StructureListModel>
    {
        [SerializeField] private InfiniteScrollView scrollView;

        private void Start()
        {
            scrollView.SetScrollAction((start, count)
                => InvokeAction(nameof(ConstructionController.ConstructionList), start, count));
        }

        private void OnEnable()
        {
            scrollView.Adjust();
        }

        protected override void UpdateViewInternal()
        {
            var scrollItems = scrollView.EnumerateItems();
            var model = GetModel();
            scrollView.SetTotalItemCount(model.totalCount);
            int structureCount = model.structures.Count();

            // Set all items of scroll view
            for (int i = 0; i < scrollView.PageSize; i++)
            {
                var item = scrollView[i];

                if (item.IsNull())
                    return;
                
                // If item has model
                if (i < structureCount)
                {
                    // Set item as active
                    item.gameObject.SetActive(true);

                    // Set model of the item
                    item.Model = model.structures.ElementAt(i);
                    item.UpdateView();      // Update child view
                }
                else
                {
                    // Otherwise, set item as inactive
                    item.gameObject.SetActive(false);
                }

            }
        }

        /// <summary>
        /// Returns the maximum number of elements that can be displayed on the viewport
        /// </summary>
        /// <returns></returns>
        public int GetPageSize()
        {
            return scrollView.PageSize;
        }
    }
}
