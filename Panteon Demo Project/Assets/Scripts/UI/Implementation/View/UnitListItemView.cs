﻿using PanteonDemo.Events.EventTypes;
using PanteonDemo.Events.Interface;
using PanteonDemo.UI.Base;
using PanteonDemo.UI.Model;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PanteonDemo.UI.View
{
    public class UnitListItemView : TemplateView<UnitModel>
    {
        [SerializeField] private Image imageView;

        private IEventModule m_EventModule;

        private void Start()
        {
            m_EventModule = FindObjectOfType<IEventModule>();
        }

        protected override void UpdateViewInternal()
        {
            imageView.sprite = GetModel().unitSprite;
        }

        public void OnClick()
        {
            var model = GetModel();

            // Raise unit list item clicked event
            m_EventModule.Raise(new UnitListItemClicked() {
                UnitName = model.unitName,
                UnitType = model.unitType
            });
        }
    }
}
