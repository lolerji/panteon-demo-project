﻿using PanteonDemo.Data;
using PanteonDemo.UI.Base;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.UI.Model
{
    public class StructureModel : ModelBase
    {
        public string structureName;

        public string structureDescription;

        public Structures structureType;

        public Sprite structureSprite;
    }
}
