﻿using PanteonDemo.UI.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.UI.Model
{
    public class ProductionListModel : ModelBase
    {
        public string structureDescriptor;

        public IEnumerable<UnitModel> units;

        public int totalCount;
    }
}
