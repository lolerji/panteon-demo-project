﻿using PanteonDemo.UI.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.UI.Model
{
    public class StructureListModel : ModelBase
    {
        public IEnumerable<StructureModel> structures;

        public int totalCount;
    }
}
