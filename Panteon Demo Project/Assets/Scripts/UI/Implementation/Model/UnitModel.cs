﻿using PanteonDemo.Data;
using PanteonDemo.UI.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.UI.Model
{
    public class UnitModel : ModelBase
    {
        public string unitName;

        public Sprite unitSprite;

        public Units unitType;
    }
}
