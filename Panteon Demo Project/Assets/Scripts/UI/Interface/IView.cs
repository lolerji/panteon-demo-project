﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.UI.Interface
{
    public abstract class IView : MonoBehaviour
    {
        public abstract void UpdateView();

        public abstract IModel Model { get; set; }

        public abstract IController Controller { get; set; }
    }
}