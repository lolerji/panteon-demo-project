﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.UI.Interface
{
    public interface IModel
    {
        void AttachView(IView view);

        void NotifyViews();
    }
}
