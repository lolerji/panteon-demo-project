﻿using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using UnityEngine;

namespace PanteonDemo.UI.Interface
{
    public interface IController
    {
        void DispatchModel(IView view, IModel model);

        void InvokeAction(string actionName, object[] args);
    }
}
