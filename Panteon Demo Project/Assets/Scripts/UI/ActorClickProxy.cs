﻿using PanteonDemo.Components;
using PanteonDemo.Events.EventTypes;
using PanteonDemo.Events.Interface;
using PanteonDemo.UI.Base;
using PanteonDemo.UI.Controller;
using PanteonDemo.UI.View;
using UnityEngine;

namespace PanteonDemo.UI
{
    public class ActorClickProxy : MonoBehaviour
    {
        [Header("Construction View")]
        [SerializeField] private ConstructionController constructionController;
        [SerializeField] private InfiniteScrollView constructionListView;

        [Header("Structure Details View")]
        [SerializeField] private StructureController structureController;
        [SerializeField] private InfiniteScrollView productionScrollView;

        private IEventModule m_EventModule;

        private void OnEnable()
        {
            m_EventModule = m_EventModule ?? FindObjectOfType<IEventModule>();
            m_EventModule.AddListener<ActorClicked>(OnActorClick);
        }

        private void OnDisable()
        {
            m_EventModule = m_EventModule ?? FindObjectOfType<IEventModule>();
            m_EventModule.RemoveListener<ActorClicked>(OnActorClick);
        }

        private void OnActorClick(ActorClicked data)
        {
            switch (data.Actor.GetActorType())
            {
                case Data.ActorType.Structure:
                    StructureClick(data.GameObject.GetComponent<ActorComponent>());
                    break;
            }
        }

        private void StructureClick(ActorComponent structure)
        {
            // Call structure controller
            structureController.InvokeAction(nameof(StructureController.StructureDetails), structure.Info.GetDescriptor());
            structureController.InvokeAction(nameof(StructureController.ProductionList), structure.Info.GetDescriptor(), 0, productionScrollView.PageSize);
        }
    }
}