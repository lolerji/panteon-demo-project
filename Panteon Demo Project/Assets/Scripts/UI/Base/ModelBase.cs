﻿using PanteonDemo.UI.Interface;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.UI.Base
{
    public abstract class ModelBase : IModel
    {
        private List<IView> m_Views;

        public void AttachView(IView view)
        {
            m_Views = m_Views ?? new List<IView>();
            if (!m_Views.Contains(view))
                m_Views.Add(view);
            view.Model = this;
        }

        public void NotifyViews()
        {
            foreach (var view in m_Views)
            {
                view.UpdateView();
            }
        }

        public static ModelBase Null { get { return NullModel.Instance; } }
    }

    sealed class NullModel : ModelBase, INull
    {
        private static NullModel m_Instance;

        private NullModel() { }

        public static NullModel Instance
        {
            get
            {
                m_Instance = m_Instance ?? new NullModel();
                return m_Instance;
            }
        }
    }
}
