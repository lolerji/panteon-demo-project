﻿using PanteonDemo.UI.Interface;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Dynamic;
using System.Linq;

namespace PanteonDemo.UI.Base
{
    public abstract class ControllerBase : MonoBehaviour, IController
    {
        private Dictionary<string, InvocationMeta> m_Invocations;

        public void DispatchModel(IView view, IModel model)
        {
            view.Controller = this;
            model.AttachView(view);
            model.NotifyViews();
        }

        /// <summary>
        /// Invokes an action method from this controller
        /// </summary>
        /// <param name="actionName"></param>
        /// <param name="args"></param>
        public abstract void InvokeAction(string actionName, params object[] obj);
    }

    class InvocationMeta
    {
        public Delegate method;

        public string[] argNames;
    }
}
