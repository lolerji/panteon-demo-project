﻿using PanteonDemo.UI.Interface;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using UnityEngine;

namespace PanteonDemo.UI.Base
{
    public abstract class TemplateView<TModel> : IView
        where TModel : class, IModel
    {
        protected TModel GetModel()
        {
            return Model as TModel;
        }

        /// <summary>
        /// Invoke the action identified by actionName with given parameter package
        /// </summary>
        /// <param name="actionName"></param>
        /// <param name="args"></param>
        protected void InvokeAction(string actionName, params object[] args)
        {
            Controller.InvokeAction(actionName, args);
        }

        protected abstract void UpdateViewInternal();

        public override void UpdateView()
        {
            // If view is inactive when it is updated
            // with a valid model make it active
            if (!gameObject.activeInHierarchy && !Model.IsNull())
            {
                gameObject.SetActive(true);
            }
            else if (Model.IsNull())
            {
                // Otherwise, if model is null,
                // close view
                gameObject.SetActive(false);
                return;
            }

            UpdateViewInternal();
        }

        public override IModel Model { get; set; }

        public override IController Controller { get; set; }
    }
}
