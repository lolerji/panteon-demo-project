﻿using PanteonDemo.Components;
using PanteonDemo.Grid;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo
{
    public static class Extensions
    {
        /// <summary>
        /// Prevent UnityEngine.Object's "null" not being recognized as "null"
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool IsNull(this object obj)
        {
            if (obj is INull)
            {
                return true;
            }
            else
            {
                return obj == null || obj.Equals(null);
            }
        }

        /// <summary>
        /// Compares a layer to the bits of a layer mask and checks if it is set in the layer mask
        /// </summary>
        /// <param name="mask"></param>
        /// <param name="layer"></param>
        /// <returns></returns>
        public static bool MatchLayer(this LayerMask mask, int layer)
        {
            return (mask.value & 1 << layer) != 0;
        }

        public static void SetTiledTransformPosition(this TiledTransformComponent tiledObject, IGrid grid, Vector3 position)
        {
            var xOffset = Vector3.zero;
            var yOffset = Vector3.zero;

            var widthVector = Vector3.Scale(tiledObject.Dimensions, grid.GetXAxis());
            var heightVector = Vector3.Scale(tiledObject.Dimensions, grid.GetYAxis());

            // Apply offset to position if meta is even
            // so when using the returned position,
            // object is aligned with grid perfectly
            if (widthVector.magnitude % 2 == 0 && widthVector.magnitude > 1)
                xOffset = grid.GetXAxis() * grid.GetCellSize().x / 2;

            if (heightVector.magnitude % 2 == 0 && heightVector.magnitude > 1)
                yOffset = grid.GetYAxis() * grid.GetCellSize().y / 2;

            tiledObject.transform.position = position + xOffset + yOffset;
        }

        public static Vector3 Round(this Vector3 vector)
        {
            vector.x = Mathf.RoundToInt(vector.x);
            vector.y = Mathf.RoundToInt(vector.y);
            vector.z = Mathf.RoundToInt(vector.z);

            return vector;
        }
    }
}
