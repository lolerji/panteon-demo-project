﻿using PanteonDemo.Containers;
using PanteonDemo.Grid;
using PanteonDemo.PathFinding.Interface;
using Priority_Queue;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.PathFinding
{
    public class AStarActorGridPathFinder : IPathFinder
    {
        private IGrid m_Grid;
        private IActorContainer m_ActorContainer;
        
        private LayerMask m_ObstacleLayers;
        private Vector3 m_Up;

        #region Singleton

        private static AStarActorGridPathFinder m_Instance;

        private AStarActorGridPathFinder() { }
        
        /// <summary>
        /// Creates or gets the singleton instance of A* Path Finder
        /// </summary>
        /// <returns></returns>
        public static AStarActorGridPathFinder Singletonize()
        {
            if (m_Instance.IsNull())
            {
                m_Instance = CreateInstance<AStarActorGridPathFinder>();
            }

            return m_Instance;
        }

        #endregion
        
        /// <summary>
        /// Sets required fields for the pathfinder to work correctly
        /// with actors and grid system
        /// </summary>
        /// <param name="layerMask"></param>
        /// <param name="upwardsVector"></param>
        public void ConfigurePathFinder(LayerMask layerMask, Vector3 upwardsVector)
        {
            m_ObstacleLayers = layerMask;
            m_Up = upwardsVector;
        }

        public override IEnumerable<Vector3> FindPath(Vector3 startPosition, Vector3 endPosition)
        {
            // Find dependencies
            m_Grid = m_Grid ?? FindObjectOfType<IGrid>();
            m_ActorContainer = m_ActorContainer ?? FindObjectOfType<IActorContainer>();

            // There must be an actor container and grid!
            if (m_Grid.IsNull() || m_ActorContainer.IsNull())
                return null;

            var cameFrom = new Dictionary<Vector3, Vector3>();
            var costSoFar = new Dictionary<Vector3, double>();
            var frontier = new SimplePriorityQueue<Vector3, double>();

            // Find cells corresponding to start and end positions
            var startCellPosition = m_Grid.Cell(startPosition);
            var endCellPosition = m_Grid.Cell(endPosition);

            // Initial step
            frontier.Enqueue(startCellPosition, 0);
            cameFrom[startCellPosition] = startCellPosition;
            costSoFar[startCellPosition] = 0;

            // Loop till frontier becomes empty or 
            // search finds the target
            while (frontier.Count > 0)
            {
                var current = frontier.Dequeue();

                // Found the goal or is the goal and obstacle
                if (current == endCellPosition || IsObstacle(endCellPosition))
                {
                    break;
                }

                foreach (var next in m_Grid.Neighbours(current))
                {
                    // If next position is an obstacle
                    if (IsObstacle(next))
                        continue;       // Skip to other neighbours

                    double gCost = costSoFar[current] + 1;

                    // If next position is not yet calculated or
                    // it is calculated but new value has a better gCost
                    if (!costSoFar.ContainsKey(next) || gCost < costSoFar[next])
                    {
                        costSoFar[next] = gCost;

                        // F cost will be used as priority, it is gCost + hCost
                        double fCost = gCost + Heuristic(current, endCellPosition);
                        frontier.Enqueue(next, fCost);      // Expand frontier to next

                        // We go to next from current
                        // current <- next
                        cameFrom[next] = current;
                    }
                }
            }

            var path = new Stack<Vector3>();
            var currentPosition = endCellPosition;
            Vector3 temp;

            // Loop till start and add every single position
            // Between start point and end point to the stack
            //
            // Stack because we are looping backwards, but
            // object will need to loop forward, it makes it easier
            while (cameFrom.TryGetValue(currentPosition, out temp) && currentPosition != temp)
            {
                path.Push(currentPosition);
                currentPosition = cameFrom[currentPosition];
            }

            return path;
        }

        /// <summary>
        /// Calculates the distance between 2 position vectors
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        private double Heuristic(Vector3 start, Vector3 end)
        {
            return Vector3.Distance(start, end);
        }

        /// <summary>
        /// Checks if there is an obstacle at given position
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        private bool IsObstacle(Vector3 position)
        {
            foreach (var actor in m_ActorContainer.EnumerateActors())
            {
                // Does position have an obstacle
                if (actor.BindingVolume.bounds.IntersectRay(new Ray(position, m_Up)) && actor.Info.obstacle)
                    return true;
            }

            return false;
        }
    }
}
