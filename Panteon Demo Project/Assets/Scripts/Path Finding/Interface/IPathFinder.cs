﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.PathFinding.Interface
{
    public abstract class IPathFinder : ScriptableObject
    {
        /// <summary>
        /// Finds the best path from start position to end position
        /// </summary>
        /// <param name="startPosition"></param>
        /// <param name="endPosition"></param>
        /// <returns></returns>
        public abstract IEnumerable<Vector3> FindPath(Vector3 startPosition, Vector3 endPosition);
    }
}
