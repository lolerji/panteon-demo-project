﻿using PanteonDemo;
using PanteonDemo.Components;
using PanteonDemo.Data;
using PanteonDemo.Factory.Interface;
using PanteonDemo.Grid;
using PanteonDemo.Pooling;
using UnityEngine;

public class GridTest : MonoBehaviour
{
    [SerializeField] private Camera mainCamera;
    [SerializeField] private PreviewBoxComponent previewBox;

    private IGrid m_Grid;
    private IActorPool m_ActorPool;

    private void Start()
    {
        m_Grid = FindObjectOfType<IGrid>();
        m_ActorPool = FindObjectOfType<IActorPool>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            // Get a new actor instance
            var actor = m_ActorPool.RequestObject(Structures.Barracks.ToString());

            // Pass new actor to preview box for placement and previewing
            previewBox.SetPreviewActor(actor.GetComponent<ActorComponent>());
            previewBox.Activate();      // Activate preview box
        }
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            // Deactivate preview box
            previewBox.Deactivate();

            // Return actor that is being previewed, if there is one
            var previewing = previewBox.GetPreviewActor();
            if (!previewing.IsNull())
            {
                previewBox.Deactivate();
                previewBox.SetPreviewActor(null);
                m_ActorPool.ReturnObject(previewing.Info.GetDescriptor(), previewing.gameObject);
            }
        }
    }
}
