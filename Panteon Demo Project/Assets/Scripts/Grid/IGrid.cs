﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.Grid
{
    public abstract class IGrid : ScriptableObject
    {
        /// <summary>
        /// Returns the position of the closest cell to the given position
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public abstract Vector3 Cell(Vector3 position);

        /// <summary>
        /// Returns an enumerated list of cells that is neighbouring to the cell closest to the
        /// given position
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public abstract IEnumerable<Vector3> Neighbours(Vector3 position);

        /// <summary>
        /// Sets the x axis of the grid
        /// </summary>
        /// <param name="x"></param>
        public abstract void SetXAxis(Vector3 x);

        /// <summary>
        /// Gets x axis direction vector
        /// </summary>
        public abstract Vector3 GetXAxis();

        /// <summary>
        /// Sets the y axis of the grid
        /// </summary>
        /// <param name="y"></param>
        public abstract void SetYAxis(Vector3 y);

        /// <summary>
        /// Gets y axis direction vector
        /// </summary>
        public abstract Vector3 GetYAxis();

        /// <summary>
        /// Sets origin point of the grid
        /// </summary>
        /// <param name="origin"></param>
        public abstract void SetGridOrigin(Vector3 origin);

        /// <summary>
        /// Adjusts the size of each cell in the grid
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public abstract void SetCellSize(float width, float height);

        /// <summary>
        /// Returns the cell size
        /// </summary>
        /// <returns></returns>
        public abstract Vector3 GetCellSize();
    }
}
