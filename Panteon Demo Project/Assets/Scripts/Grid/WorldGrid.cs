﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.Grid
{
    public class WorldGrid : IGrid
    {
        private bool m_Updated;

        private Vector3 m_CellSize;

        private Vector3 m_Origin;
        private Vector3 m_XAxis;
        private Vector3 m_YAxis;

        private readonly List<Vector3> m_Directions;

        #region Singleton

        private static WorldGrid m_Instance;

        private WorldGrid()
        {
            m_Directions = new List<Vector3>();

            m_Origin = Vector3.zero;
            m_XAxis = Vector3.forward;
            m_YAxis = Vector3.right;

            m_CellSize = new Vector3(1, 1, 0);

            m_Updated = true;
        }

        /// <summary>
        /// Creates an instance of World Grid or returns an existing instance
        /// </summary>
        /// <returns>The singleton instance of World Grid</returns>
        public static WorldGrid Singletonize()
        {
            if (m_Instance.IsNull())
            {
                m_Instance = CreateInstance<WorldGrid>();
            }

            return m_Instance;
        }

        #endregion
        
        public override Vector3 Cell(Vector3 position)
        {
            // Calculate cell position and return it
            Vector3 x = Vector3.Scale(m_XAxis, position) / m_CellSize.x;
            Vector3 y = Vector3.Scale(m_YAxis, position) / m_CellSize.y;
            return m_Origin + x.Round() + y.Round();
        }

        public override IEnumerable<Vector3> Neighbours(Vector3 position)
        {
            // Recalculate direction vectors if grid was updated
            if (m_Updated)
            {
                GenerateDiections();
            }

            // Find closest cell to the position
            Vector3 cellPosition = Cell(position);

            // Enumerate all neighbours
            foreach (var direction in m_Directions)
            {
                yield return cellPosition + direction;
            }
        }

        public override void SetCellSize(float width, float height)
        {
            m_CellSize.x = width;
            m_CellSize.y = height;
            m_Updated = true;
        }

        public override Vector3 GetCellSize()
        {
            return m_CellSize;
        }

        public override void SetXAxis(Vector3 x)
        {
            m_XAxis = x;
            m_Updated = true;
        }

        public override Vector3 GetXAxis()
        {
            return m_XAxis;
        }

        public override void SetYAxis(Vector3 y)
        {
            m_YAxis = y;
            m_Updated = true;
        }

        public override Vector3 GetYAxis()
        {
            return m_YAxis;
        }

        public override void SetGridOrigin(Vector3 origin)
        {
            m_Origin = origin;
        }

        /// <summary>
        /// Used to calculate direction vectors for finding neighbour cells
        /// should be called once the grid's cell sizes or axes are updated
        /// </summary>
        private void GenerateDiections()
        {
            m_Directions.Clear();

            //   
            //  ----- + ----- + -----
            //        |       |
            //        |  0,1  | 
            //        |       |
            //  ----- + ----- + -----
            //        |       |
            //  -1,0  | cell  |  1,0
            //        |       |
            //  ----- + ----- + -----
            //        |       |
            //        |  0,-1 |  
            //        |       |
            //  ----- + ----- + -----
            //
            m_Directions.Add(m_XAxis * -m_CellSize.x);
            m_Directions.Add(m_YAxis * -m_CellSize.y);
            m_Directions.Add(m_XAxis * m_CellSize.x);
            m_Directions.Add(m_YAxis * m_CellSize.y);

            m_Updated = false;
        }
    }
}
