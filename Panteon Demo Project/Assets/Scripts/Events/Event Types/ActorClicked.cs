﻿using PanteonDemo.Components;
using PanteonDemo.Data;
using PanteonDemo.Events.Interface;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.Events.EventTypes
{
    public class ActorClicked : IEvent
    {
        public void Use()
        {
            IsUsed = true;
        }

        public bool IsUsed { get; private set; }

        public ActorInfo Actor { get; set; }

        public GameObject GameObject { get; set; }
    }
}
