﻿using PanteonDemo.Data;
using PanteonDemo.Events.Interface;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.Events.EventTypes
{
    public class UnitListItemClicked : IEvent
    {
        public void Use()
        {
            IsUsed = true;
        }

        public bool IsUsed { get; private set; }

        public string UnitName { get; set; }

        public Units UnitType { get; set; }
    }
}