﻿using PanteonDemo.Data;
using PanteonDemo.Events.Interface;

namespace PanteonDemo.Events.EventTypes
{
    public class StructureListItemClicked : IEvent
    {
        public void Use()
        {
            IsUsed = true;
        }

        public bool IsUsed { get; private set; }

        public string StructureName { get; set; }

        public Structures StructureType { get; set; }
    }
}
