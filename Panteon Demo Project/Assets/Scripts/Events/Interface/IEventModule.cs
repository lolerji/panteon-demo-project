﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.Events.Interface
{
    public delegate void EventDelegate<TEvent>(TEvent data)
        where TEvent : IEvent;

    public abstract class IEventModule : ScriptableObject
    {
        /// <summary>
        /// Initializes event module
        /// </summary>
        public abstract void InitializeModule();

        /// <summary>
        /// Raises the specified event with given data
        /// </summary>
        /// <typeparam name="TEvent"></typeparam>
        /// <param name="data"></param>
        public abstract void Raise<TEvent>(TEvent data)
            where TEvent : IEvent;

        /// <summary>
        /// Adds a callback method for the specified event type
        /// </summary>
        /// <typeparam name="TEvent"></typeparam>
        /// <param name="callback"></param>
        public abstract void AddListener<TEvent>(EventDelegate<TEvent> callback)
            where TEvent : IEvent;

        /// <summary>
        /// Removes an existing event callback method from the specified event type
        /// </summary>
        /// <typeparam name="TEvent"></typeparam>
        /// <param name="callback"></param>
        public abstract void RemoveListener<TEvent>(EventDelegate<TEvent> callback)
            where TEvent : IEvent;

        /// <summary>
        /// Clears all event delegates
        /// </summary>
        public abstract void ClearAllListeners();
    }
}
