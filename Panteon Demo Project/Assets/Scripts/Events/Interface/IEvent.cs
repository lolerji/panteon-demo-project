﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.Events.Interface
{
    public interface IEvent
    {
        void Use();

        bool IsUsed { get; }
    }
}