﻿using PanteonDemo.Events.Interface;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.Events
{
    public class EventModule : IEventModule
    {
        private Dictionary<System.Type, System.Delegate> m_Delegates;

        #region Singleton

        private static EventModule m_Instance;

        private EventModule()
        {
            InitializeModule();
        }
        
        /// <summary>
        /// Creates or gets the singleton instance of Event Module
        /// </summary>
        /// <returns></returns>
        public static EventModule Singletonize()
        {
            if (m_Instance.IsNull())
            {
                m_Instance = CreateInstance<EventModule>();
            }

            return m_Instance;
        }

        #endregion

        public override void Raise<TEvent>(TEvent data)
        {
            System.Delegate d = null;

            if (m_Delegates.TryGetValue(typeof(TEvent), out d))
            {
                // Convert delegate reference into correct format
                var eventDelegate = d as EventDelegate<TEvent>;

                // Make sure conversion succeeded
                if (eventDelegate.IsNull())
                    throw new UnityException("Corrupted event delegate for: " + typeof(TEvent).Name);

                // Invoke event delegate
                eventDelegate.Invoke(data);
            }
        }

        public override void AddListener<TEvent>(EventDelegate<TEvent> callback)
        {
            System.Delegate eventDelegate = null;
            
            if (m_Delegates.TryGetValue(typeof(TEvent), out eventDelegate))
            {
                // Combine new event delegate with existing event delegate
                m_Delegates[typeof(TEvent)] = System.Delegate.Combine(eventDelegate, callback);
            }
            else
            {
                // Add callback as the first delegate for the specified event type
                m_Delegates[typeof(TEvent)] = callback;
            }
        }

        public override void InitializeModule()
        {
            m_Delegates = new Dictionary<System.Type, System.Delegate>();
        }

        public override void RemoveListener<TEvent>(EventDelegate<TEvent> callback)
        {
            System.Delegate d = null;

            if (m_Delegates.TryGetValue(typeof(TEvent), out d))
            {
                // Remove callback invocation from delegate's invocation list
                var currentDelegate = System.Delegate.Remove(d, callback);

                if (currentDelegate.IsNull())
                {
                    // If delegate has no invocations remaining, remove event type from dictionary
                    m_Delegates.Remove(typeof(TEvent));
                }
                else
                {
                    // Set updated delegate as the event delegate
                    m_Delegates[typeof(TEvent)] = currentDelegate;
                }
            }
        }

        public override void ClearAllListeners()
        {
            m_Delegates.Clear();
        }
    }
}
