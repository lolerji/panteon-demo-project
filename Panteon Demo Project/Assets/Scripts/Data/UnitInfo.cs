﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.Data
{
    [CreateAssetMenu(fileName = "New Unit Info", menuName = "Actors/Unit")]
    public class UnitInfo : ActorInfo
    {
        public Sprite sprite;

        public Units unitType;

        public float speed;

        public override string GetDescriptor()
        {
            return actorName;
        }

        public override ActorType GetActorType()
        {
            return ActorType.Unit;
        }
    }

    public enum Units
    {
        Destroyer,
        LightCruiser,
        HeavyCruiser,
        Battleship,
        Carrier
    }
}
