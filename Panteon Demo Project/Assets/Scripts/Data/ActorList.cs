﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.Data
{
    public class ActorList<TActorType> : ScriptableObject
        where TActorType : ActorInfo
    {
        public List<TActorType> actors;
    }

    [CreateAssetMenu(fileName = "New Structure List", menuName = "Actor List/Structure List")]
    public class StructureList : ActorList<StructureInfo> { }

    [CreateAssetMenu(fileName = "New Unit List", menuName = "Actor List/Unit List")]
    public class UnitList : ActorList<UnitInfo> { }
}
