﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.Data
{
    public abstract class ActorInfo : ScriptableObject
    {
        /// <summary>
        /// Name of the actor
        /// </summary>
        public string actorName;

        /// <summary>
        /// Prefab of the actor
        /// </summary>
        public GameObject prefab;

        /// <summary>
        /// Total hit points of the actor
        /// </summary>
        public int HP;

        /// <summary>
        /// Determines if the unit is an obstacle (B)
        /// </summary>
        public bool obstacle;

        /// <summary>
        /// Dimensions of the actor
        /// </summary>
        public Vector3 dimensions;

        /// <summary>
        /// Description of the actor
        /// </summary>
        [TextArea(3, 10)]
        public string description;

        public abstract string GetDescriptor();

        public abstract ActorType GetActorType();
    }

    public enum ActorType
    {
        Structure,
        Unit
    }
}
