﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PanteonDemo.Data
{
    [CreateAssetMenu(fileName = "New Structure Info", menuName = "Actors/Structure")]
    public class StructureInfo : ActorInfo
    {
        public Sprite sprite;

        public Structures structureType;

        public bool canProduceUnits;

        public UnitInfo[] produces;

        public override string GetDescriptor()
        {
            return actorName;
        }

        public override ActorType  GetActorType()
        {
            return ActorType.Structure;
        }
    }

    public enum Structures
    {
        Barracks,
        PowerPlant
    }
}
